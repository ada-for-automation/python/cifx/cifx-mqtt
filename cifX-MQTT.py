#!/usr/bin/env python3

"""
 This Python script implements a Hilscher cifX to MQTT Gateway

 Tutorial :
 http://www.steves-internet-guide.com/mqtt-python-beginners-course/
 
 Reference :
 https://pypi.org/project/paho-mqtt/
"""

import signal
import struct
import time
import os

import paho.mqtt.client as mqttc

from cffi_cifx_linux import ffi as ffilinux, lib as liblinux
from cffi_cifx import ffi, lib

from cifX_misc import \
    int8_to_hex, int16_to_hex, int32_to_hex, \
    Linux_cifXGetDriverVersion, Log_Error, cifXGetDriverVersion

MQTT_Connection_Status = \
[
    "Connection successful",                              # 0
    "Connection refused - incorrect protocol version",    # 1
    "Connection refused - invalid client identifier",     # 2
    "Connection refused - server unavailable",            # 3
    "Connection refused - bad username or password",      # 4
    "Connection refused - not authorised",                # 5
    "Currently unused"                                    # 6 - 255
]

running = True

def quit():
    global running
    running = False
    print("Main : User interrupt !")

def sig_handler(signum, frame):
    quit()

def on_log(client, userdata, level, buf):
    print("MQTT Client log :", buf)

def on_connect(client, userdata, flags, rc):
    print("MQTT :", MQTT_Connection_Status[rc])
    
    if rc == 0 :
        print("MQTT : Subscribe to topics")
        client.subscribe(
        [
          ("cifX/Hello", 0),
          ("ps7/s7-315-2dp/mw0", 0)
        ])

        print("MQTT : Say hello !")
        client.publish("cifX/Hello", "True")

def on_disconnect(client, userdata, rc):
    if rc != 0 :
        print("MQTT : Unexpected disconnection.")
    else :
        print("MQTT : Disconnected.")

def on_message(client, userdata, message):
    print("MQTT : Got message :",
          "\n\tpayload\t\t",    str(message.payload.decode("utf-8")),
          "\n\ttopic\t\t",      message.topic,
          "\n\tqos\t\t",        message.qos,
          "\n\tretain flag\t",  message.retain)

    if message.topic == "ps7/s7-315-2dp/mw0" :
        userdata["S7_MW0"] = int(str(message.payload.decode("utf-8")))
        print("MQTT : S7_MW0 = ", userdata["S7_MW0"])


def main():
    """
    Hilscher cifX to MQTT Gateway Main
    """

    print("=======================================\n"
          "  Hilscher cifX to MQTT Gateway Main ! \n"
          "=======================================\n")

    signal.signal(signal.SIGINT, sig_handler)

    CIFX_DRIVER_INIT_AUTOSCAN = 1

    OldResult = 0
    Result = 0
    info = {}

    cifX_Init_Done = False
    cifX_Driver_Open_Done = False
    cifX_Channel_Open_Done = False

    inDataWord0 = 0
    inDataWord1 = 0

    BROKER_HOST = os.environ.get("MQTT_BROKER_HOST", "localhost")

    MyData = {"S7_MW0" : 0}

    pInit_params = ffilinux.new("struct CIFX_LINUX_INIT*")

    pInit_params.init_options = CIFX_DRIVER_INIT_AUTOSCAN

    print("Main : Initialising Linux Driver")
    Result = liblinux.cifXDriverInit(pInit_params)

    if Result != 0 :
        print("Error cifXDriverInit :", int32_to_hex(Result))
    else:
        cifX_Init_Done = True

    if cifX_Init_Done:
        Linux_cifXGetDriverVersion(info)

    if cifX_Init_Done:
        phDriver = ffi.new("CIFXHANDLE*")

        print("Main : Opening Driver")
        Result = lib.xDriverOpen(phDriver)

        if Result != OldResult :
            OldResult = Result
            Log_Error("xDriverOpen", Result)

        if Result == 0 :
            cifX_Driver_Open_Done = True

    if cifX_Driver_Open_Done:
        cifXGetDriverVersion(phDriver[0], info)

    if cifX_Driver_Open_Done:
        phChannel = ffi.new("CIFXHANDLE*")
        pBoard = ffi.new("char[]", b"cifX0")

        print("Main : Opening Channel")
        Result = lib.xChannelOpen(phDriver[0], pBoard, 0, phChannel)

        if Result != OldResult :
            OldResult = Result
            Log_Error("xChannelOpen", Result)

        if Result == 0 :
            cifX_Channel_Open_Done = True

    print("Main : cifX Information:\n", info)

    print("MQTT : Creating an instance of MQTT Client")
    mqtt_client = mqttc.Client(mqttc.CallbackAPIVersion.VERSION1,
                              "cifX", userdata = MyData)

    print("MQTT : Connecting handlers")
    mqtt_client.on_log = on_log
    mqtt_client.on_connect = on_connect
    mqtt_client.on_disconnect = on_disconnect
    mqtt_client.on_message = on_message

    print("MQTT : Connecting to broker :", BROKER_HOST)
    mqtt_client.connect(BROKER_HOST)

    print("MQTT : Starting loop")
    mqtt_client.loop_start()

    if cifX_Channel_Open_Done:

        print("===================================\n"
              "   IO Loop : Ctrl-c to terminate !\n"
              "===================================\n")

        pInData = ffi.new("uint8_t[20]")
        pOutData = ffi.new("uint8_t[20]")
        print("sizeof(pInData) =", ffi.sizeof(pInData))

        while running:

            Result = \
              lib.xChannelIORead(phChannel[0],        # Channel Handle
                                 0,                   # AreaNumber
                                 0,                   # Offset
                                 ffi.sizeof(pInData), # DataLen
                                 pInData,             # Data
                                 10)                  # Timeout

            if Result != OldResult :
                OldResult = Result
                Log_Error("xChannelIORead", Result)

            if Result == 0 :

                mqtt_client.publish("cifX/Byte[0]", pInData[0])

                inDataWord0, inDataWord1 = \
                                  struct.unpack('>HH', ffi.buffer(pInData)[0:4])

                mqtt_client.publish("cifX/Word0", inDataWord0)

                pOutData[0:2] = struct.pack('>h', MyData["S7_MW0"])

                Result = \
                  lib.xChannelIOWrite(phChannel[0],         # Channel Handle
                                      0,                    # AreaNumber
                                      0,                    # Offset
                                      ffi.sizeof(pOutData), # DataLen
                                      pOutData,             # Data
                                      10)                   # Timeout

                if Result != OldResult :
                    OldResult = Result
                    Log_Error("xChannelIOWrite", Result)

            print("pInData[0]), pInData[1]",
                  int8_to_hex(pInData[0]), int8_to_hex(pInData[1]))
            print("inDataWord0", int16_to_hex(inDataWord0))
            print("Main : S7_MW0 = ", MyData["S7_MW0"])
            time.sleep(1.05)

    if cifX_Channel_Open_Done:
        print("Main : Closing Channel")
        Result = lib.xChannelClose(phChannel[0])
        cifX_Channel_Open_Done = False

        if Result != OldResult :
            OldResult = Result
            Log_Error("xChannelClose", Result)

    if cifX_Driver_Open_Done:
        print("Main : Closing Driver")
        Result = lib.xDriverClose(phDriver[0])
        cifX_Driver_Open_Done = False

        if Result != OldResult :
            OldResult = Result
            Log_Error("xDriverClose", Result)

    if cifX_Init_Done:
        print("Main : Linux Driver DeInit")
        liblinux.cifXDriverDeinit()
        cifX_Init_Done = False

    print("MQTT : Say Bye !")
    mqtt_client.publish("cifX/Hello", "False")

    print("MQTT : Unsubscribe from topics")
    mqtt_client.unsubscribe(
    [
      "cifX/Hello",
      "ps7/s7-315-2dp/mw0"
    ])

    print("MQTT : Wait a few seconds...")
    time.sleep(5)

    print("MQTT : Stoping loop")
    mqtt_client.loop_stop()

    print("MQTT : Disconnecting from broker")
    mqtt_client.disconnect()

if __name__ == "__main__":
    main()


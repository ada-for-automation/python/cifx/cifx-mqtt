# syntax=docker/dockerfile:1

# Build image with :
# docker build --tag=python-cifx-mqtt:stable .

# Create container : \
# docker create --privileged --restart=no \
# --name python-cifx-mqtt \
# --volume /sys:/sys \
# --volume /home/admin/Test_cifXDrv/opt/cifx:/opt/cifx \
# --env MQTT_BROKER_HOST=10.252.253.1 \
# python-cifx-mqtt:stable

# Start container :
# docker start python-cifx-mqtt

# Use latest debian stable as base image
FROM debian:stable

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian(stable) / Python / cifX / MQTT"

# Version
ENV PYTHON_CIFX_MQTT_VERSION 0.0.1

# Execute all commands as root
USER root

# Install cifX Driver
COPY --from=x86-64-test-cifx-dev:stable  \
    /usr/local/lib/libcifx.so.3.0.0 /usr/local/lib/

# Install necessary stuff for cifX Driver and Python binding
RUN apt-get update \
    && apt-get install -y \
          libpciaccess0 \
          python3 python3-venv \
    && ldconfig

WORKDIR /app

# cifX Python Binding path
ENV LOCATION="/home/test/Python/cifX"

# Install cifX Python Binding
COPY --from=x86-64-test-cifx-dev:stable  \
    $LOCATION/cffi_cifx*.so \
    $LOCATION/cifX_misc.py ./

# Install application
COPY ./cifX-MQTT.py ./

# Create a virtual environment, activate it and install requirements
RUN python3 -m venv ./venv \
    && . ./venv/bin/activate \
    && python3 -m pip install cffi \
    && python3 -m pip install paho-mqtt

# Do entrypoint
ENTRYPOINT . ./venv/bin/activate \
           && python cifX-MQTT.py


